This git contains the tutorials of the [Ecole Evry Schatzman 2023 school on Stellar Physics with Gaia](https://ees2023.sciencesconf.org):


* TP1: Gaia archive (Carine Babusiaux): GUI [description](TP1_GACS.pdf) + [python notebook](TP1_GACS.ipynb)
* TP2: Gaia spectra (Carine Babusiaux): discover using TOPCAT [pdf](TP2_GaiaSpectra.pdf) + [python notebook](TP2_GaiaSpectra.ipynb)
* TP3: Open clusters (Orlagh Creevey): [python notebook](TP3_OpenClusters.ipynb)
* TP4: Multiple stars (Frédéric Arenou / Nicolas Leclerc) : [python notebook](TP4_NSS_masses.ipynb)
* TP5: Variable stars (Laurent Eyer) : [python notebook](TP5_variables.ipynb) 

Preliminary installations needed:

* [user registration](https://www.cosmos.esa.int/web/gaia-users/register) to the [Gaia archive](https://gea.esac.esa.int/archive/)
* [TOPCAT](https://www.star.bris.ac.uk/~mbt/topcat/)
* [Jupyter notebooks](https://jupyter.org/) !
* [astroquery](https://astroquery.readthedocs.io/en/latest/#installation) (pip install astroquery)
* [GaiaXPy](https://gaiaxpy.readthedocs.io/en/latest/installation.html) (pip install GaiaXpy)
* [ezbasti](https://github.com/dinilbose/ezbasti) (pip install ezbasti / if it does not work with your python version: git clone https://github.com/dinilbose/ezbasti.git ; cd ezbasti ; pip install .)
* [nsstools](https://gitlab.obspm.fr/gaia/nsstools) (pip install nsstools)
* [sympy](https://www.sympy.org/) (pip install sympy)
